package com.example.reactor.service.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.reactor.model.NotificationData;
import com.example.reactor.service.NotificationService;

import reactor.bus.Event;
import reactor.fn.Consumer;

/**
 *  Author : Do Van Khoi
 *  Email  : khoidv@fsoft.com.vn
 *  Jun 26, 2018
 */

@Service
public class NotificationConsumer implements Consumer<Event<NotificationData>> {

  @Autowired
  private NotificationService notificationService;

  @Override
  public void accept(Event<NotificationData> notificationDataEvent) {

    NotificationData notificationData = notificationDataEvent.getData();
    try {
      notificationService.initiateNotification(notificationData);
    } catch (InterruptedException e) {
    }

  }

}
