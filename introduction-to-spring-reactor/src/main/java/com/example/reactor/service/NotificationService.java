/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.reactor.service;

import com.example.reactor.model.NotificationData;

/**
 *  Author : Do Van Khoi
 *  Email  : khoidv@fsoft.com.vn
 *  Jun 26, 2018
 */
public interface NotificationService {
  
  void initiateNotification(NotificationData notificationData) throws InterruptedException;
  
}
