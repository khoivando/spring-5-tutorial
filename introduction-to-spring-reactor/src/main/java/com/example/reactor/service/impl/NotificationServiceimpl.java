/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.reactor.service.impl;

import org.springframework.stereotype.Service;

import com.example.reactor.model.NotificationData;
import com.example.reactor.service.NotificationService;

/**
 *  Author : Do Van Khoi
 *  Email  : khoidv@fsoft.com.vn
 *  Jun 26, 2018
 */
@Service
public class NotificationServiceimpl implements NotificationService {

  @Override
  public void initiateNotification(NotificationData notificationData) throws InterruptedException {
    System.out.println("Notification service started for "+ "Notification ID: " + notificationData.getId());
    Thread.sleep(5000);
    System.out.println("Notification service ended for "+ "Notification ID: " + notificationData.getId());
  }

}
