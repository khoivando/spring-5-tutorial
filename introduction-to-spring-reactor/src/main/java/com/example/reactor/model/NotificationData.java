/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.reactor.model;

/**
 *  Author : Do Van Khoi
 *  Email  : khoidv@fsoft.com.vn
 *  Jun 26, 2018
 */
public class NotificationData {
  private long id;
  private String name;
  private String email;
  private String mobile;
  public NotificationData() {
    super();
  }
  public long getId() {
    return id;
  }
  public void setId(long id) {
    this.id = id;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public String getMobile() {
    return mobile;
  }
  public void setMobile(String mobile) {
    this.mobile = mobile;
  }
  
}
