/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.reactor.model.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.reactor.model.NotificationData;

import reactor.bus.Event;
import reactor.bus.EventBus;

/**
 *  Author : Do Van Khoi
 *  Email  : khoidv@fsoft.com.vn
 *  Jun 26, 2018
 */
@Controller
public class NotificationController {

  private @Autowired EventBus eventBus;

  @GetMapping("/startNotification/{param}")
  public void startNotification(@PathVariable Integer param) {
    for (int i = 0; i < param; i++) {

      NotificationData data = new NotificationData();
      data.setId(i);

      eventBus.notify("notificationConsumer", Event.wrap(data));

      System.out.println("Notification " + i + ": notification task submitted successfully");
    }
  }

}
