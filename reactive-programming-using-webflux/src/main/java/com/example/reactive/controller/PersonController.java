/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.reactive.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.reactive.model.Person;
import com.example.reactive.repository.PersonRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Aug 31, 2018
 */
@RestController
@RequestMapping(path = "person")
public class PersonController {

  private @Autowired PersonRepository personRepository;
  
  private static final Logger logger = LoggerFactory.getLogger(PersonController.class);
  
  @GetMapping(path = "{id}")
  public Mono<Person> get(@PathVariable Long id) {
    logger.info("get person by id: {}", id);
    Optional<Person> optional = personRepository.findById(id);
    return Mono.just(optional.orElse(Person.EMPTY));
  }
  
  @GetMapping(path = "all")
  public Flux<Person> listAll() {
    List<Person> persons = personRepository.findAll();
    Flux<Person> flux = Flux.fromIterable(persons);
    return flux;
  }
}
