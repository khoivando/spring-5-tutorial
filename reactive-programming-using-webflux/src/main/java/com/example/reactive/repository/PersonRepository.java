/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.reactive.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.reactive.model.Person;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Aug 31, 2018
 */
public interface PersonRepository extends JpaRepository<Person, Long> {

}
