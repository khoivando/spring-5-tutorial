/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.reactive.test;

import org.junit.Test;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Aug 31, 2018
 */
public class Tester4ExceptionTests {  
  
  @Test(expected = Exception.class)
  public void exception() throws Exception {
    throw new Exception("Test Exception");
  }
  
  @Test(timeout = 1)
  public void timeout() throws InterruptedException {
    Thread.sleep(10);
  }

}
