/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.reactive.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Aug 31, 2018
 */
public class Tester5AssumptionTests {
  
  @Test
  public void testOnProd() {
    System.setProperty("ENV", "DEV");
    
    Assumptions.assumingThat("DEV".equals(System.getProperty("ENV")), () -> {
      Assertions.assertTrue(false);
    });
  }
  
}
