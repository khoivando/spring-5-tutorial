/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.reactive.test;

import java.time.Duration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Aug 31, 2018
 */
public class Tester5ExceptionTests {

  @Test
  public void testException() throws Exception {
    Assertions.assertThrows(Exception.class, () -> {throw new Exception("Test exception");});
  }

  @Test
  public void timeout() {
    Assertions.assertTimeout(Duration.ofMillis(10), () -> Thread.sleep(5)); 
  }

}
