/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.reactive.test;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.example.reactive.ReactiveApplication;


/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Aug 31, 2018
 */
@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReactiveApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class TesterRunnerTests {

  private @Autowired WebTestClient webClient;

  @Test
  public void monoClientTest() {

    webClient.get().uri("person/{0}", 1)
    .accept(MediaType.APPLICATION_JSON)
    .exchange()
    .expectStatus().isOk()
    .expectBody()
    .jsonPath("$.name").isNotEmpty()
    .jsonPath("$.name").isEqualTo("bryan");
  }
  
  @Test 
  public void fluxClientTest() {
    webClient.get().uri("person/all")
    .accept(MediaType.APPLICATION_JSON)
    .exchange()
    .expectStatus().isOk()
    .expectBody()
    .consumeWith(response -> org.assertj.core.api.Assertions.assertThat(response.getResponseBody()).isNotNull());
  }

}
