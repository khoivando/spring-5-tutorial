/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.exception;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 12, 2018
 */
@SuppressWarnings("serial")
public class TweetNotFoundException extends RuntimeException {
  
  public TweetNotFoundException(String tweetId) {
    super("\"Tweet not found with id " + tweetId);
  }
  
}
