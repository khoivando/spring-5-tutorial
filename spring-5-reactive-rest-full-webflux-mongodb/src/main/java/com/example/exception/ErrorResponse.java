/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.exception;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 12, 2018
 */
public class ErrorResponse {
  
  private String message;

  public ErrorResponse(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
  
}
