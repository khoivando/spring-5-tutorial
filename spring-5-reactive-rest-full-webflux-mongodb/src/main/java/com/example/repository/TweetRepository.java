/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.example.model.Tweet;

import reactor.core.publisher.Flux;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 12, 2018
 */
@Repository
public interface TweetRepository extends ReactiveMongoRepository<Tweet, String> {
  
  @Query("{ id: { $exists: true }}")
  Flux<Tweet> retrieveAllTweetPage(final Pageable pageable);

}
