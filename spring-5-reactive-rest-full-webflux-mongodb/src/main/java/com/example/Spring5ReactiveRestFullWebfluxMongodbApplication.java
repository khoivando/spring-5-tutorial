package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring5ReactiveRestFullWebfluxMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(Spring5ReactiveRestFullWebfluxMongodbApplication.class, args);
	}
}
