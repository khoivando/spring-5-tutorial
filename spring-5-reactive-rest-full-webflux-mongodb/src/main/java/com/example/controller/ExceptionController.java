/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.controller;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import com.example.exception.ErrorResponse;
import com.example.exception.TweetNotFoundException;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 12, 2018
 */
@RestController
@ControllerAdvice
public class ExceptionController {

  @SuppressWarnings("unused")
  @ExceptionHandler(DuplicateKeyException.class)
  public ResponseEntity<?> handleDuplicateKeyException(DuplicateKeyException ex) {
    return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Tweet with the same text already exists"));
  }

  @SuppressWarnings("unused")
  @ExceptionHandler(TweetNotFoundException.class)
  public ResponseEntity<?> handleTweetNotFoundException(TweetNotFoundException ex) {
    return ResponseEntity.notFound().build();
  }

}
