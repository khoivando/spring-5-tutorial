/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Tweet;
import com.example.repository.TweetRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 12, 2018
 */
@RestController
@RequestMapping(value = "tweet")
public class TweetController {

  private @Autowired TweetRepository tweetRepository;

  @GetMapping(value = "all",  produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  public Flux<Tweet> getAllTweets() {
    return tweetRepository.findAll();
    //PageRequest pageable = new PageRequest(1, 2);
    //return tweetRepository.retrieveAllTweetPage(pageable);
  }

  @GetMapping("/{id}")
  public Mono<ResponseEntity<Tweet>> get(@PathVariable(value = "id") String tweetId) {
    return tweetRepository.findById(tweetId)
        .map(savedTweet -> ResponseEntity.ok(savedTweet))
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @PostMapping("add")
  public Mono<Tweet> createTweets(@Valid @RequestBody Tweet tweet) {
    return tweetRepository.save(tweet);
  }

  @PutMapping("update/{id}")
  public Mono<ResponseEntity<Tweet>> update(@PathVariable(value = "id") String tweetId, @Valid @RequestBody Tweet tweet) {
    return tweetRepository.findById(tweetId)
        .flatMap(existingTweet -> {
          existingTweet.setText(tweet.getText());
          return tweetRepository.save(existingTweet);
        })
        .map(updateTweet -> ResponseEntity.ok(updateTweet))
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @DeleteMapping("delete/{id}")
  public Mono<ResponseEntity<Void>> deleteTweet(@PathVariable(value = "id") String tweetId) {
    return tweetRepository.findById(tweetId)
        .flatMap(existingTweet -> tweetRepository.delete(existingTweet)
            .then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
        .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

}
