/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 3, 2018
 */
public class AbstractController<G> {

  protected @Autowired G gateway;
  protected Logger logger = LoggerFactory.getLogger(getClass());

  protected ResponseEntity<?> toResult(Object object) {
    return new ResponseEntity<>(object, HttpStatus.OK);
  }

}
