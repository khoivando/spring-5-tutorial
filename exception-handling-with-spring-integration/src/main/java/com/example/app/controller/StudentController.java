/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.app.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.app.exception.NotFoundException;
import com.example.app.gateway.StudentGateway;
import com.example.app.model.Student;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 3, 2018
 */
@RestController
@RequestMapping("student")
public class StudentController extends AbstractController<StudentGateway> {

  @PostMapping("add") 
  public ResponseEntity<?> add(@Valid @RequestBody Student studentRequest) {
    Student student = gateway.add(studentRequest);
    return toResult(student);
  }

  @GetMapping("all")
  public List<Student> all() {
    return gateway.findAll();
  }

  @GetMapping("{id}")
  public ResponseEntity<?> get(@PathVariable Long id) {
    Optional<Student> optional = gateway.get(id);
    if(optional.isPresent()) {
      return toResult(optional.get());
    }
    throw new NotFoundException("Could not find student by id "+ id);
  }
  
  @DeleteMapping("{id}")
  public ResponseEntity<?> delete(@PathVariable Long id) {
    return null;
  }

}
