/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.app.controller;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.app.exception.BadRequestException;
import com.example.app.exception.ErrorResponse;
import com.example.app.exception.NotFoundException;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 3, 2018
 */
@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
  
  
  @ExceptionHandler(BadRequestException.class)
  public final ResponseEntity<ErrorResponse> handleBadRequestException(BadRequestException e, WebRequest request) throws IOException {
    ErrorResponse body = new ErrorResponse(
        HttpStatus.BAD_REQUEST.value(), 
        HttpStatus.BAD_REQUEST.getReasonPhrase(), 
        e.getMessage(), 
        request.getDescription(true));
    
    logger.info("getParameterMap " + ((ServletWebRequest)request).getRequest());
    logger.info("getHeaderNames " + request.getHeaderNames().toString());
    logger.info("afdf " + ((ServletWebRequest)request).getRequest().getRequestURL().toString());
    
    return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(NotFoundException.class)
  public final ResponseEntity<ErrorResponse> handleUserNotFoundException(NotFoundException e, WebRequest request) {
    ErrorResponse body = new ErrorResponse(
        HttpStatus.NOT_FOUND.value(), 
        HttpStatus.NOT_FOUND.getReasonPhrase(), 
        e.getMessage(), 
        request.getDescription(true));
    return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(Exception.class)
  public final ResponseEntity<ErrorResponse> handleAllExceptions(Exception e, WebRequest request) {
    ErrorResponse body = new ErrorResponse(
        HttpStatus.INTERNAL_SERVER_ERROR.value(), 
        HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), 
        e.getMessage(), 
        request.getDescription(true));
    return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
  }

}
