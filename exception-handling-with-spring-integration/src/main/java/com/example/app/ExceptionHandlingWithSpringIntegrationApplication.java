package com.example.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("student-gateway.xml")
public class ExceptionHandlingWithSpringIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExceptionHandlingWithSpringIntegrationApplication.class, args);
	}
}
