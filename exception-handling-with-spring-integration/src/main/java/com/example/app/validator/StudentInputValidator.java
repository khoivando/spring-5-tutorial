/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.app.validator;

import org.springframework.stereotype.Service;

import com.example.app.exception.BadRequestException;
import com.example.app.model.Student;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 4, 2018
 */
@Service
public class StudentInputValidator {
  
  public Student validate4Add(Student student) throws BadRequestException {
    if(student.getName().isEmpty()) {
      throw new BadRequestException("name must not be null or empty");
    }
    if(student.getPassportNumber().isEmpty()) {
      throw new BadRequestException("passport number must not be null or empty");
    }
    return student;
  }
  
}
