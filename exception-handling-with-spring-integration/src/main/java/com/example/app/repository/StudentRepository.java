/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.app.model.Student;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 3, 2018
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
}
