/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.app.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 4, 2018
 */
public class AbstractService<T, ID extends Serializable, R extends JpaRepository<T, ID>> {

  protected @Autowired R repository;

  public T add(T entity) {
    return repository.save(entity);
  }

  public Optional<T> get(ID id) {
    return repository.findById(id);
  }

  public List<T> findAll() {
    return repository.findAll();
  }

}
