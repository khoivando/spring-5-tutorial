/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.app.service;

import org.springframework.stereotype.Service;

import com.example.app.model.Student;
import com.example.app.repository.StudentRepository;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 4, 2018
 */
@Service("studentService")
public class StudentServiceImpl extends AbstractService<Student, Long, StudentRepository> implements StudentService {

}
