/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.app.gateway;

import com.example.app.model.Student;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 4, 2018
 */
public interface StudentGateway extends CommonGateway<Student, Long> {
}
