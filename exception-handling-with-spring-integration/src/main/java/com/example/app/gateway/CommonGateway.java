/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.app.gateway;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 4, 2018
 */
public interface CommonGateway<T, ID extends Serializable> {
  
  T add(T t);
  
  Optional<T> get(ID id);
  
  List<T> findAll();
  
}
