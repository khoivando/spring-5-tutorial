/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.app.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 4, 2018
 */
@SuppressWarnings("serial")
@ResponseStatus(reason = "Invalid Request Parameters")
public class InvalidRequestException extends RuntimeException {
  
  public InvalidRequestException(String message) {
    super(message);
  }
  
}
