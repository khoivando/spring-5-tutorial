package com.example.oraclejpatemplate;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OracleJpaTemplateApplication implements CommandLineRunner {

  @Autowired
  TeamRepository teamRepository;
  
  @Autowired
  SoccerService soccerService;
  
	public static void main(String[] args) {
		SpringApplication.run(OracleJpaTemplateApplication.class, args);
	}

  @Override
  public void run(String... args) throws Exception {
    Team team = teamRepository.getOne(1l);
    Player player = new Player("Eto", 9, "Forward", team);
    
    soccerService.addPlayer(player);
    List<Player> players = soccerService.getPlayers(team.getId());
    System.out.println(players);
  }

}

