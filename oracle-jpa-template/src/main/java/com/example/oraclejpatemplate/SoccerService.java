/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.oraclejpatemplate;

import java.util.List;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 11, 2019
 */
public interface SoccerService {
  
  Player addPlayer(Player player);
  
  List<Player> getPlayers(Long teamId);
  
}
