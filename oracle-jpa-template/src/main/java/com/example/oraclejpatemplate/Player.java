/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.oraclejpatemplate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 11, 2019
 */
@Entity
public class Player {

  @Id
  @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "player_Sequence")
  @SequenceGenerator(name = "player_Sequence", sequenceName = "PLAYER_SEQ")
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "num")
  private int num;

  @Column(name = "position")
  private String position;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "team_id", nullable = false)
  private Team team;

  public Player() {
  }

  public Player(String name, int num, String position, Team team) {
    super();
    this.name = name;
    this.num = num;
    this.position = position;
    this.team = team;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getNum() {
    return num;
  }

  public void setNum(int num) {
    this.num = num;
  }

  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }

  public Team getTeam() {
    return team;
  }

  public void setTeam(Team team) {
    this.team = team;
  }

  @Override
  public String toString() {
    return "Player [id=" + id + ", name=" + name + ", num=" + num + ", position=" + position+ ", team=" + team + "]";
  }

}
