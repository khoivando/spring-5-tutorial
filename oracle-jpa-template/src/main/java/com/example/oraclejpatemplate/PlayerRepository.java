/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.oraclejpatemplate;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 11, 2019
 */
public interface PlayerRepository extends JpaRepository<Player, Long> {

  List<Player> findByTeamId(Long teamId);

}
