/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.oraclejpatemplate;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 11, 2019
 */
@Service
public class SoccerServiceImpl implements SoccerService {
  
  @Autowired
  private PlayerRepository playerRepository;
  
  @Override
  public Player addPlayer(Player player) {
    return playerRepository.save(player);
  }

  @Override
  public List<Player> getPlayers(Long teamId) {
    return playerRepository.findByTeamId(teamId);
  }
  
}
