/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.Employee;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 3, 2019
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
  
  Optional<Employee> findByFirstName(String firstName);
  
}
