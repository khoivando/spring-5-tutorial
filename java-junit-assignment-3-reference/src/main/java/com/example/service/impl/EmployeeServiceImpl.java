/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.service.impl;

import com.example.model.Employee;
import com.example.repository.EmployeeRepository;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 3, 2019
 */
public class EmployeeServiceImpl extends AbstractServiceImpl<Employee, Long, EmployeeRepository> {

}
