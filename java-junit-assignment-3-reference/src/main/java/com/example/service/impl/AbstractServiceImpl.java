/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.service.impl;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 3, 2019
 */
public class AbstractServiceImpl<T, ID extends Serializable, R extends JpaRepository<T, ID>> {

}
