/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.service;

import java.io.Serializable;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 3, 2019
 */
public interface CommonService<T, ID extends Serializable> {
  
  T add();
  
  T get(ID id);
  
  T update(ID id);
  
  void delete(ID id);
  
}
