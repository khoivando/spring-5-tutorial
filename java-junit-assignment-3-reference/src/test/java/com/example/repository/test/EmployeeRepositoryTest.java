/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.repository.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.model.Employee;
import com.example.repository.EmployeeRepository;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 3, 2019
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeRepositoryTest {

  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  private EmployeeRepository employeeRepository;

  @Test
  public void whenFindByFistName_thenReturnEmployee() {
    // given
    Employee employee = new Employee("do", "van khoi", 32, "0976896988");
    entityManager.persist(employee);
    entityManager.flush();

    // when
    Optional<Employee> optional = employeeRepository.findByFirstName(employee.getFirstName());
    // then
    assertThat(optional.get().getFirstName()).isEqualTo(employee.getFirstName());
  }

}
