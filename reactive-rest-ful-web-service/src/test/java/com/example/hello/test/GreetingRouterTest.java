package com.example.hello.test;


import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.example.hello.Application;

@RunWith(SpringRunner.class)
//  We create a `@SpringBootTest`, starting an actual server on a `RANDOM_PORT`
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = Application.class)
public class GreetingRouterTest {

  // Spring Boot will create a `WebTestClient` for you,
  // already configure and ready to issue requests against "localhost:RANDOM_PORT"
  @Autowired
  private WebTestClient webTestClient;

  @Test
  //@Ignore
  public void testHello() {
    webTestClient
    // Create a GET request to test an endpoint
    .get().uri("/hello")
    .accept(MediaType.TEXT_PLAIN)
    .exchange()
    // and use the dedicated DSL to test assertions against the response
    .expectStatus().isOk()
    .expectBody(String.class).isEqualTo("Hello, Spring!");
  }
  
  @Test
  //@Ignore
  public void testHello2() {
    webTestClient
    // Create a GET request to test an endpoint
    .get().uri("/hello2")
    .accept(MediaType.TEXT_PLAIN)
    .exchange()
    // and use the dedicated DSL to test assertions against the response
    .expectStatus().isOk()
    .expectBody(String.class).isEqualTo("Hello, Spring5!");
  }
}