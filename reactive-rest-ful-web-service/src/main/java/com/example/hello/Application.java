package com.example.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.basic.ExampleWebClient;

@SpringBootApplication
@EnableAutoConfiguration
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		
		GreetingWebClient gwc = new GreetingWebClient();
    System.out.println(gwc.getResult());
    
    ExampleWebClient client = new ExampleWebClient();
    System.out.println(client.getResult());
    
	}
}
