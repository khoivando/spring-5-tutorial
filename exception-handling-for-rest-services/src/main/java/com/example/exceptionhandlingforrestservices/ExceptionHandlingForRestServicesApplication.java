package com.example.exceptionhandlingforrestservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExceptionHandlingForRestServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExceptionHandlingForRestServicesApplication.class, args);
	}
}
