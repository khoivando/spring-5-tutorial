/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.exceptionhandlingforrestservices;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *  Author : Author
 *          Email:email@fsoft.com.vn
 * Dec 3, 2018
 */
@RestController
public class StudentController {
  
  @Autowired
  private StudentRepository studentRepository;
  
  @GetMapping("/students")
  public List<Student> retrieveAllStudents() {
    System.out.println("========>>>> call here ");
    return studentRepository.findAll();
  }
  
  @GetMapping("/students/{id}")
  public Resource<Student> retrieveStudent(@PathVariable long id) {
    
    Optional<Student> student = studentRepository.findById(id);
    if (!student.isPresent()) throw new StudentNotFoundException("Could not find student with id " + id);

    Resource<Student> resource = new Resource<Student>(student.get());
    ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).retrieveAllStudents());
    resource.add(linkTo.withRel("all-students"));

    return resource;
  }
  
  @DeleteMapping("/students/{id}")
  public void deleteStudent(@PathVariable long id) {
    studentRepository.deleteById(id);
  }
  
  @PostMapping("/students")
  public ResponseEntity<Object> createStudent(@RequestBody Student student) {
    Student savedStudent = studentRepository.save(student);

    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedStudent.getId()).toUri();
    return ResponseEntity.created(location).build();
  }
  
  @PutMapping("/students/{id}")
  public ResponseEntity<Object> updateStudent(@RequestBody Student student, @PathVariable long id) {

    Optional<Student> studentOptional = studentRepository.findById(id);
    if (!studentOptional.isPresent()) return ResponseEntity.notFound().build();
    studentRepository.save(student);

    return ResponseEntity.ok(student);
  }
  

}
