/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.integration.test;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.example.si.SITemplateApplication;
import com.example.si.model.Student;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 14, 2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SITemplateApplication.class)
@AutoConfigureMockMvc
public class StudentControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Test
  @Ignore
  public void testAdd() throws Exception {
    Student student = new Student("Nguyen Van DEF", "0976896911", "email@g.com");

    mockMvc.perform(post("/student/add")
        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(new ObjectMapper().writeValueAsString(student)))
    .andExpect(status().isCreated())
    .andExpect(jsonPath("$.name", is("Nguyen Van DEF")));
  }

  @Test
  public void whenAddStudentWithInvalidPhone_thenReturnBadRequest() throws Exception {
    Student student = new Student("Nguyen Van DEF", "097689691", "email@g.com");

    mockMvc.perform(post("/student/add")
        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(new ObjectMapper().writeValueAsString(student)))
    .andDo(MockMvcResultHandlers.print())
    .andExpect(status().isBadRequest());
  }

  @Test
  public void whenGetNotExistStudent_thenReturnNotFound() throws Exception {
    Long id = -1l;
    mockMvc.perform(get("/student/get/"+ id))
    .andDo(MockMvcResultHandlers.print())
    .andExpect(status().isNotFound());    
  }


}
