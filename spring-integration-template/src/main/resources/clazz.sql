create database clazz;
use clazz;

CREATE TABLE `student` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  -- `class_id` bigint(20) not null,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` smallint NOT NULL,
   PRIMARY KEY (`id`)
) ;

CREATE TABLE `class` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL, 
   PRIMARY KEY (`id`)
) ;

insert into class(id, `name`) values(1, 'spring class 01');

select * from student;

