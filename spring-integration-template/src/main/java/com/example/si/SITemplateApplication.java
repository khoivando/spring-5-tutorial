package com.example.si;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;

@SpringBootApplication
//@ComponentScan(basePackages = {"com.example.si.*"})
//@ImportResource("classpath:application-config.xml")
public class SITemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(SITemplateApplication.class, args);
	}

	@Bean(name = "dispatchChannel")
	public MessageChannel initDispatchChannel() {
	  return new DirectChannel();
	}
	
}

