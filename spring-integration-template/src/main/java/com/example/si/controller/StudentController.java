/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.si.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.si.gateway.StudentGateway;
import com.example.si.model.Student;
import com.example.si.model.request.StudentRequest;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 11, 2019
 */
@RestController
@RequestMapping("student")
public class StudentController {

  private @Autowired StudentGateway gateway;

  @PostMapping
  public ResponseEntity<?> add(@RequestBody StudentRequest request) {
    Student student = gateway.add(request);
    return new ResponseEntity<>(student, HttpStatus.CREATED);
  }

  @GetMapping("{id}")
  public ResponseEntity<?> get(@PathVariable Long id) {
    Student student = gateway.get(id);
    return new ResponseEntity<>(student, HttpStatus.OK);
  }

}
