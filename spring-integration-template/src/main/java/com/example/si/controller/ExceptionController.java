/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.si.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.si.exception.ErrorResponse;
import com.example.si.exception.InvalidPhoneNumberException;
import com.example.si.exception.NotFoundDataException;


/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 14, 2019
 */
@RestController
@RestControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

  @ExceptionHandler(NotFoundDataException.class)
  public ResponseEntity<?> handleEntityNotFoundException(NotFoundDataException e, WebRequest webRequest) {
    ErrorResponse body = new ErrorResponse(
        HttpStatus.NOT_FOUND.value(), 
        HttpStatus.NOT_FOUND.getReasonPhrase(), 
        e.getMessage(), 
        webRequest.getDescription(false));
    System.out.println("===>>> handleEntityNotFoundException called() ");
    return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
  }
  
  @ExceptionHandler(InvalidPhoneNumberException.class) 
  public ResponseEntity<?> handleInvalidPhoneNumberException(InvalidPhoneNumberException e, WebRequest webRequest) {
    ErrorResponse body = new ErrorResponse(
        HttpStatus.BAD_REQUEST.value(), 
        HttpStatus.BAD_REQUEST.getReasonPhrase(), 
        e.getMessage(), 
        webRequest.getDescription(false));
    System.out.println("===>>> handleInvalidPhoneNumberException called() ");
    return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
  }
  
}
