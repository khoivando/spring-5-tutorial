/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.si.service;

import java.io.Serializable;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 15, 2019
 */
public interface CommonService<T, ID extends Serializable> {
  
  T add(T t);

  T update(T t);

  T get(ID id);
  
  Boolean exist(ID id);

  void remove(ID id);
  
}
