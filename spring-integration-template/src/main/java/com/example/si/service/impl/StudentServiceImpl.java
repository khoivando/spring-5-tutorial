/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.si.service.impl;

import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;

import com.example.si.exception.NotFoundDataException;
import com.example.si.model.Student;
import com.example.si.repository.StudentRepository;
import com.example.si.service.StudentService;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 11, 2019
 */
@Service("studentService")
public class StudentServiceImpl extends AbstractServiceImpl<Student, Long, StudentRepository> implements StudentService {

  private @Autowired MessageChannel dispatchChannel;
  
  @Override
  @ServiceActivator(inputChannel = "student-add-channel-service")
  public Student add(Student student) {
    
    ExecutorService executor = Executors.newFixedThreadPool(1);
    executor.execute(() -> dispatchChannel.send(MessageBuilder.withPayload(student).build()));
    executor.shutdown();
    
    return repository.save(student);
  }

  @Override
  public Student update(Student student) {
    return repository.save(student);
  }

  @Override
  @ServiceActivator(inputChannel = "student-get-channel")
  public Student get(Long id) {
    logger.info("get student by id "+ id);
    Optional<Student> optinal = repository.findById(id);
    if(optinal.isPresent()) return optinal.get();
    throw new NotFoundDataException("Could not find student by id "+ id);
  }

  @Override
  public Boolean exist(Long id) {
    return repository.existsById(id);
  }

  @Override
  public void remove(Long id) {
    repository.deleteById(id);
  }

}
