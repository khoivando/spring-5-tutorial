/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.si.service.impl;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 15, 2019
 */
public class AbstractServiceImpl<T, ID extends Serializable, R extends JpaRepository<T, ID>> {
  
  protected @Autowired R repository;
  protected Logger logger;
  
  public AbstractServiceImpl() {
    logger = LoggerFactory.getLogger(getClass());
  }
  
}
