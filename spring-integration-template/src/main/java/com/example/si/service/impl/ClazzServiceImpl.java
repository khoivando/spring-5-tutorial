/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.si.service.impl;

import java.util.Optional;

import com.example.si.exception.NotFoundDataException;
import com.example.si.model.Clazz;
import com.example.si.repository.ClazzRepository;
import com.example.si.service.ClazzService;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 15, 2019
 */
public class ClazzServiceImpl extends AbstractServiceImpl<Clazz, Long, ClazzRepository> implements ClazzService {

  @Override
  public Clazz add(Clazz t) {
    return repository.save(t);
  }

  @Override
  public Clazz update(Clazz t) {
    return repository.save(t);
  }

  @Override
  public Clazz get(Long id) {
    logger.debug("get clazz by id "+ id);
    Optional<Clazz> optinal = repository.findById(id);
    if(optinal.isPresent()) return optinal.get();
    throw new NotFoundDataException("Could not find clazz by id "+ id);
  }

  @Override
  public Boolean exist(Long id) {
    return repository.existsById(id);
  }

  @Override
  public void remove(Long id) {
    repository.deleteById(id);
  }

}
