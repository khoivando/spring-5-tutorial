/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.si.service.impl;

import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import com.example.si.model.Student;
import com.example.si.service.DispatchService;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 15, 2019
 */
@Service
public class DispatchServiceImpl implements DispatchService {

  @Override
  @ServiceActivator(inputChannel = "dispatchChannel")
  public void dispatch(Student student) throws InterruptedException {
    Thread.sleep(20*1000);
    System.out.println("===>>> dispatch student");
  }

}
