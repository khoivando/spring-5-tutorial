/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.si.service;

import com.example.si.model.Student;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 15, 2019
 */
public interface DispatchService {
  
  void dispatch(Student student) throws InterruptedException;
  
}
