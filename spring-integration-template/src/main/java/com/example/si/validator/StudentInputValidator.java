/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.si.validator;

import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import com.example.si.exception.InvalidPhoneNumberException;
import com.example.si.model.request.StudentRequest;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 14, 2019
 */
@Service
public class StudentInputValidator {

  @ServiceActivator(inputChannel = "student-add-channel-validate-input", outputChannel = "student-add-channel-transformer")
  public StudentRequest validate4Add(StudentRequest request) {
    System.out.println("validate student input");
    if(request.getPhone().length() < 10) {
      throw new InvalidPhoneNumberException("Phone invalid of length");
    }
    return request;
  }

}
