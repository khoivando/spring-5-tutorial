/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.si.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 15, 2019
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentRequest {
  
  private Long id;
  private String name;
  private String phone;
  private String email;
  private Short status;
  
}
