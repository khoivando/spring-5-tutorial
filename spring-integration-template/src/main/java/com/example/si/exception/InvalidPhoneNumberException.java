/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.si.exception;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 14, 2019
 */
@SuppressWarnings("serial")
public class InvalidPhoneNumberException extends RuntimeException {
  
  public InvalidPhoneNumberException(String message) {
    super(message);
  }
  
}
