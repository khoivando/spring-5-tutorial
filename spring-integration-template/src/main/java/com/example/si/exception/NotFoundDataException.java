/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.si.exception;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 11, 2019
 */
@SuppressWarnings("serial")
public class NotFoundDataException extends RuntimeException {

  public NotFoundDataException(String message) {
    super(message);
  }

}
