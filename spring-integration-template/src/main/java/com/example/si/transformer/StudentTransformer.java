/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.si.transformer;

import org.springframework.core.convert.converter.Converter;
import org.springframework.integration.annotation.Transformer;
import org.springframework.stereotype.Component;

import com.example.si.model.Student;
import com.example.si.model.request.StudentRequest;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 15, 2019
 */
@Component
public class StudentTransformer implements Converter<StudentRequest, Student> {

  @Override
  @Transformer(inputChannel = "student-add-channel-transformer", outputChannel = "student-add-channel-service")
  public Student convert(StudentRequest source) {
    System.out.println("==>> convert studentRequest -> student entity");
    Student student = new Student(source.getName(), source.getPhone(), source.getEmail());
    student.setStatus((short) 1);
    return student;
  }
}
