/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.si.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.si.model.Student;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 11, 2019
 */
public interface StudentRepository extends JpaRepository<Student, Long> {
}
