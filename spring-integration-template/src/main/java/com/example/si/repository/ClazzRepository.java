/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.si.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.si.model.Clazz;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 15, 2019
 */
public interface ClazzRepository extends JpaRepository<Clazz, Long> {

}
