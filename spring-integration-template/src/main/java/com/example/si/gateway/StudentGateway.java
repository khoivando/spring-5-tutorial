/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.si.gateway;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

import com.example.si.model.Student;
import com.example.si.model.request.StudentRequest;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 * Jan 11, 2019
 */
@MessagingGateway(name = "studentGateway")
public interface StudentGateway {
  
  @Gateway(requestChannel = "student-add-channel-validate-input")
  Student add(StudentRequest studentRequest);

  Student update(Student student);

  @Gateway(requestChannel = "student-get-channel")
  Student get(Long id);
  
  Boolean exist(Long id);

  void remove(Long id);

}
